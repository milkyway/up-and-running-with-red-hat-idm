Up and Running with Red Hat Identity Manager

Presenters

Jim Wildman, Principal Solution Architect, Southeast Region

Chuck Mattern, Principal Solution Architect, Southeast Region

Jeremiah Buckley, Senior Solutions Architect, Southeast Region

Abstract:

Red Hat Identity Management (IDM) can play a central role in user authentication, authorization and control. It can manage critical security components such as ssh keys, host based access controls and selinux contexts. These functions can be provided in a standalone environment or in a trust relationship with a Microsoft Active Domain Controller. 

Audience/Intro/Prerequisites:

This lab is geared towards system administrators and architects who want to gain a basic understanding of IDM installation and deployment  

Attendees, during this session, will

* Perform basic installation and configuration of IDM
* User and host groups
* Sudo rules
* Ssh keys
* Host based access control
* Configuration of a replica
* Kerberized NFS home directories (automount)

We will use the web UI for demos and the command line interface for most of the lab exercises.

Overview

* All exercises will be conducted in a Ravello provided environment on AWS.
* The lab environments have both an external DNS schema (\{name}-<GUID>.rhpds.opentlc.com) and an internal DNS schema (\{name}.example.com)

* GUID will be provided during the lab
* \{name} is one of

* bastion - for accessing the environment

* ssh root@bastion-<GUID>.rhpds.opentlc.com

* server1 - first IdM server, due to issues we had last year, this one is already installed 

* server2 - second IdM server, will be available from the web once IdM is installed
* client1, client2, client3, client4 - RHEL 7 client machines
* client5, client6 - RHEL 8 client machines

* The internal schema is only available from within the Ravello environment and is for use between your individual lab machines. Inside your lab environment each machine is available as \{name}.example.com

* You can use \{name}.example.com for sshing between the hosts, but all IdM operations need to use the FQDN format of \{name}-<GUID>.rhpds.opentlc.com

* You will need to utilize a terminal session from the local machine to the bastion host, and then from the bastion host to the individual machines
* VM’s point to one local repo on bastion.example.com 

* You are welcome to work ahead, but please don’t ask questions ahead of where we are.
* root password on all RHEL machines is:  r3dh4t1!
* For clarity, all commands to be entered will be in bold red and all empty new lines will be shown as <ENTER>
* Before beginning, verify that forward and reverse DNS works for server1, server2 and client1 through client6 of rhpds.opentlc.com. Remember you must log into your bastion and then access each of the servers and clients from there. +


|===================================================================|
[root@server1 ~]# cat /etc/resolv.conf

search localdomain rhpds.opentlc.com +
nameserver 10.0.0.1

[root@server1 ~]# host server1.example.com

server1.rhpds.opentlc.com has address 10.0.0.6

Host server2.example.com not found: 3(NXDOMAIN)

Host server2.example.com not found: 3(NXDOMAIN)

[root@server1 ~]# host 10.0.0.7

6.0.0.10.in-addr.arpa domain name pointer server1.rhpds.opentlc.com.

|===================================================================

Lab 1: IDM Installation 
~~~~~~~~~~~~~~~~~~~~~~~~

On machine server1, install and configure IdM with DNS for a local network

* Install all the needed RPM packages.

[[t.b5bf166ae5ee2392db0d649ece5249ca7e52453d]][[t.1]]

[width="100%",cols="100%",]
|==========================================
a|
[root@server1 ~]# yum -y install ipa-server

[omitted]

Complete!

|==========================================

* Run the ipa-server-install command to install the IdM server.  626630
* With this install, we will enable the automatic creation of IDM user home directories on the system.

* Set both passwords to changeme

[[t.adc16162ab6b1b07f3dbdce906789edb084be65d]][[t.2]]

[width="100%",cols="100%",]
|=================================================================================================
a|
[root@server1 ~]# ipa-server-install --mkhomedir 

The log file for this installation can be found in /var/log/ipaserver-install.log

==============================================================================

This program will set up the IPA Server.

This includes:

  * Configure a stand-alone CA (dogtag) for certificate management

  * Configure the Network Time Daemon (ntpd)

  * Create and configure an instance of Directory Server

  * Create and configure a Kerberos Key Distribution Center (KDC)

  * Configure Apache (httpd)

  * Configure the KDC to enable PKINIT

To accept the default shown in brackets, press the Enter key.

WARNING: conflicting time&date synchronization service 'chronyd' will be disabled in favor of ntpd

Do you want to configure integrated DNS (BIND)? [no]: <ENTER>

Enter the fully qualified domain name of the computer

on which you're setting up server software. Using the form

<hostname>.<domainname>

Example: master.example.com.

Server host name [server1-<GUID>.rhpds.opentlc.com]: <ENTER>

The domain name has been determined based on the host name.

Please confirm the domain name [rhpds.opentlc.com]: example.com

The kerberos protocol requires a Realm name to be defined.

This is typically the domain name converted to uppercase.

Please provide a realm name [EXAMPLE.COM]: <ENTER>

Certain directory server operations require an administrative user.

This user is referred to as the Directory Manager and has full access

to the Directory for system management tasks and will be added to the

instance of directory server created for IPA.

The password must be at least 8 characters long.

Directory Manager password: changeme

Password (confirm): changeme

The IPA server requires an administrative user, named 'admin'.

This user is a regular system account used for IPA server administration.

IPA admin password: changeme

Password (confirm): changeme

The IPA Master Server will be configured with:

Hostname:       server1.rhpds.opentlc.com

IP address(es): 10.0.0.6

Domain name:    example.com

Realm name:     EXAMPLE.COM

Continue to configure the system with these values? [no]: yes

The following operations may take some minutes to complete.

Please wait until the prompt is returned.

[OMITTED]

Configuring rhpds.opentlc.com as NIS domain.

Client configuration complete.

The ipa-client-install command was successful

==============================================================================

Setup complete

Next steps:

        1. You must make sure these network ports are open:

                TCP Ports:

                  * 80, 443: HTTP/HTTPS

                  * 389, 636: LDAP/LDAPS

                  * 88, 464: kerberos

                UDP Ports:

                  * 88, 464: kerberos

                  * 123: ntp

        2. You can now obtain a kerberos ticket using the command: 'kinit admin'

           This ticket will allow you to use the IPA tools (e.g., ipa user-add)

           and the web user interface.

Be sure to back up the CA certificates stored in /root/cacert.p12

These files are required to create replicas. The password for these

files is the Directory Manager password

|=================================================================================================

* Verify the state of the firewall on the server1 system.

[[t.f7223d37c5f8bf3699e127002439ca1687391975]][[t.3]]

[width="100%",cols="100%",]
|===========================================================================================================
a|
[root@server1 ~]# systemctl status firewalld

firewalld.service - firewalld - dynamic firewall daemon

   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)

   Active: active (running) since Sun 2018-04-22 06:51:06 EDT; 39min ago

     Docs: man:firewalld(1)

 Main PID: 534 (firewalld)

   CGroup: /system.slice/firewalld.service

           └─534 /usr/bin/python -Es /usr/sbin/firewalld --nofork --nopid

Apr 22 06:51:03 server1-<GUID>.rhpds.opentlc.com systemd[1]: Starting firewalld - dynamic firewall daemon...

Apr 22 06:51:06 server1-<GUID>.rhpds.opentlc.com systemd[1]: Started firewalld - dynamic firewall daemon.

|===========================================================================================================

* If the results from status did not show enabled then enable firewalld, ensuring that it will be started at every system boot.

[[t.d597482138b5e4616986280ed712289629b19955]][[t.4]]

[width="100%",cols="100%",]
|============================================
|[root@server1 ~]# systemctl enable firewalld
|============================================

* If firewalld is not running, start the service.

[[t.c8fd5ad2d98c14f3d6ee2a4daac52490672676fa]][[t.5]]

[width="100%",cols="100%",]
|============================================
|[root@server1 ~]# systemctl start firewalld 
|============================================

* Add port rules to the firewall so that connections to IdM are permitted. (this command needs to be copied in sections and pasted back together)

[[t.5da763546a778c33911b024c8b94f4d28ca5603f]][[t.6]]

[width="100%",cols="100%",]
|===================================================
a|
[root@server1 ~]# firewall-cmd --permanent

--add-port=\{80/tcp,443/tcp,389/tcp,636/tcp,

88/tcp,464/tcp,53/tcp,88/udp,464/udp,53/udp,123/udp}

|===================================================

* Reload the firewall so that these port exemptions are loaded

[[t.ce8faa6e78cec982fea3db3070ef434526275d96]][[t.7]]

[width="100%",cols="100%",]
|=======================================
|[root@server1 ~]# firewall-cmd --reload
|=======================================

* Your firewalld default should now appear as follows (port order in the command output may differ, simply verify that the required ports are open):

[[t.f04156db1a7f32906dbd1c74d1d20c4a0fc35d0a]][[t.8]]

[width="100%",cols="100%",]
|==========================================================================================
a|
[root@server1 ~]# firewall-cmd --list-all

public (default, active)

  interfaces: eth0

  sources:

  services: dhcpv6-client ssh

  ports: 443/tcp 80/tcp 464/tcp 88/udp 464/udp 88/tcp 123/udp 389/tcp 53/tcp 53/udp 636/tcp

  masquerade: no

  forward-ports:

  icmp-blocks:

|==========================================================================================

[[h.8qys0779lqdm]]
Verify the installed IDM Server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Open a web browser on your lab machine and login to the web console (user admin and the password you gave during installation, you will be notified that the connection is insecure, this is expected, we are using self signed certificates in this lab) for the IdM server at: https://server1-<GUID>.rhpds.opentlc.com (this is linked from your welcome page as well) and verify the following:

* That you can login
* There are no hosts nor users other than server1 and admin.

* Verify that an initial kerberos ticket can be created.  Once this ticket is obtained, we will be able to run ipa commands.

[[t.80656d000c7570f20503cc27cac865d68714d1e5]][[t.9]]

[width="100%",cols="100%",]
|===================================================================================
a|
[root@server1 ~]# kinit admin

Password for admin@rhpds.opentlc.com: changeme +
[root@server1 ~]# klist +
Ticket cache: KEYRING:persistent:0:0 +
Default principal: admin@rhpds.opentlc.com +
 +
Valid starting       Expires              Service principal +
04/22/2018 07:37:40  04/23/2018 07:37:37  krbtgt/RHPDS.OPENTLC.COM@RHPDS.OPENTLC.COM

|===================================================================================

* Validate that a host record exists for server1-<GUID>.rhpds.opentlc.com.

[[t.fb1adfba76f30a91a6d9c218f6da2fcc3163f0b9]][[t.10]]

[width="100%",cols="100%",]
|=================================================================
a|
[root@server1 ~]# ipa host-show server1-<GUID>.rhpds.opentlc.com

  Host name: server1.rhpds.opentlc.com

  Principal name: host/server1.rhpds.opentlc.com@rhpds.opentlc.com

  Password: False

  Keytab: True

  Managed by: server1.rhpds.opentlc.com

  SSH public key fingerprint: (output truncated)

|=================================================================

Congratulations, you now have a running IDM server!!!

'''''

[[h.34dzou892vfu]]
Lab 2: Install IdM Client Systems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Sidebar….on kerberos failover.  

[[t.204d34d80a44bf2984241a7b27f1eab9879276f3]][[t.11]]

[width="100%",cols="100%",]
|===================================================================
a|
[root@bastion-<GUID> ~]# nslookup -q=SRV _ldap._tcp.opentlc.com

Server:         10.0.0.1

Address:        10.0.0.1#53

Non-authoritative answer:

_ldap._tcp.opentlc.com  service = 0 100 389 ipa1.opentlc.com.

_ldap._tcp.opentlc.com  service = 0 100 389 ipa3.opentlc.com.

_ldap._tcp.opentlc.com  service = 0 100 389 ipa2.opentlc.com.

_ldap._tcp.opentlc.com  service = 0 100 389 cam.opentlc.com.

Authoritative answers can be found from:

opentlc.com     nameserver = ipa2.opentlc.com.

opentlc.com     nameserver = ipa1.opentlc.com.

opentlc.com     nameserver = ipa3.opentlc.com.

opentlc.com     nameserver = cam.opentlc.com.

cam.opentlc.com internet address = 52.2.39.130

ipa1.opentlc.com        internet address = 23.246.247.53

ipa2.opentlc.com        internet address = 23.246.247.52

ipa3.opentlc.com        internet address = 169.45.251.57

[root@bastion-<GUID> ~]# nslookup -q=SRV _kerberos._tcp.opentlc.com

Server:         10.0.0.1

Address:        10.0.0.1#53

Non-authoritative answer:

_kerberos._tcp.opentlc.com      service = 0 100 88 ipa3.opentlc.com.

_kerberos._tcp.opentlc.com      service = 0 100 88 cam.opentlc.com.

_kerberos._tcp.opentlc.com      service = 0 100 88 ipa1.opentlc.com.

_kerberos._tcp.opentlc.com      service = 0 100 88 ipa2.opentlc.com.

Authoritative answers can be found from:

opentlc.com     nameserver = ipa3.opentlc.com.

opentlc.com     nameserver = cam.opentlc.com.

opentlc.com     nameserver = ipa2.opentlc.com.

opentlc.com     nameserver = ipa1.opentlc.com.

cam.opentlc.com internet address = 52.2.39.130

ipa1.opentlc.com        internet address = 23.246.247.53

ipa2.opentlc.com        internet address = 23.246.247.52

ipa3.opentlc.com        internet address = 169.45.251.57

|===================================================================

 +

* The following commands will need to be repeated for all 5 client machines  (server2, client1, client2, client3, client4).  We have to specify the realm and domain because the hosting environment is already running its own instance of IdM using the rhpds.opentlc.com domain..

* You will see a message about the inability to perform failover.  This is expected.

[[t.39e689178ff7ebb99e76f96bceaeac0d3ed1baf9]][[t.12]]

[width="100%",cols="100%",]
|======================================================================================================================================================================================
a|
[root@server2 ~]# yum -y install ipa-client

[output omitted]

[root@server2 ~]# ipa-client-install --server server1-<GUID>.rhpds.opentlc.com --domain example.com --mkhomedir --realm=EXAMPLE.COM

Autodiscovery of servers for failover cannot work with this configuration.

If you proceed with the installation, services will be configured to always access the discovered server for all operations and will not fail over to other servers in case of failure.

Proceed with fixed values and no DNS discovery? [no]: yes

Client hostname: server1.rhpds.opentlc.com

Realm: EXAMPLE.COM

DNS Domain: example.com

IPA Server: server1.rhpds.opentlc.com

BaseDN: dc=example,dc=com

Continue to configure the system with these values? [no]: yes

[output omitted]

Client configuration complete.

The ipa-client-install command was successful

|======================================================================================================================================================================================

* Verify the hosts are in the web interface
* Verify the hosts in the CLI

[[t.321e2e67eeaeec730029c498e2e485e63387eefe]][[t.13]]

[width="100%",cols="100%",]
|==================================================================
a|
[root@server1 ~]# ipa host-find 

--------------- +
6 hosts matched +
--------------- +
 Host name: client1-<GUID>.rhpds.opentlc.com

  Principal name: host/client1.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  Principal alias: host/client1.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  SSH public key fingerprint: (truncated)

  Host name: client2-<GUID>.rhpds.opentlc.com

  Principal name: host/client2.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  Principal alias: host/client2.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  SSH public key fingerprint: (truncated)

  Host name: client3-<GUID>.rhpds.opentlc.com

  Certificate: (truncated)

  Subject: CN=client3.rhpds.opentlc.com,O=RHPDS.OPENTLC.COM

  Serial Number: 12

  Serial Number (hex): 0xC

  Issuer: CN=Certificate Authority,O=RHPDS.OPENTLC.COM

  Not Before: Fri Apr 20 21:41:02 2018 UTC

  Not After: Mon Apr 20 21:41:02 2020 UTC

  Fingerprint (SHA1): (truncated)

  Fingerprint (SHA256): (truncated)

  Principal name: host/client3.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  Principal alias: host/client3.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  SSH public key fingerprint: (truncated)

  Host name: client4-<GUID>.rhpds.opentlc.com

  Certificate: (truncated)

  Subject: CN=client4.rhpds.opentlc.com,O=RHPDS.OPENTLC.COM

  Serial Number: 11

  Serial Number (hex): 0xB

  Issuer: CN=Certificate Authority,O=RHPDS.OPENTLC.COM

  Not Before: Fri Apr 20 21:40:42 2018 UTC

  Not After: Mon Apr 20 21:40:42 2020 UTC

  Fingerprint (SHA1): (truncated)

  Fingerprint (SHA256): (truncated)

  Host name: server1-<GUID>.rhpds.opentlc.com

  Principal name: host/server1.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  Principal alias: host/server1.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  SSH public key fingerprint: (truncated)

  Host name: server2-<GUID>.rhpds.opentlc.com

  Principal name: host/server2.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  Principal alias: host/server2.rhpds.opentlc.com@RHPDS.OPENTLC.COM

  SSH public key fingerprint: (truncated)

 

---------------------------- +
Number of entries returned 6 +
----------------------------

|==================================================================

[[h.wqf8pe9dqj3x]]
Lab 3: User and group creation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* We will create users from the server1 machine.
* Add a user of your own choosing.  The password will be reset on login.

[[t.7b2a813c89d72cab921c1bbd56af29ed662027d3]][[t.14]]

[width="100%",cols="100%",]
|=====================================================
a|
[root@server1-<GUID> ~]# ipa user-add user1 --password

First name: Jim 

Last name: Wildman

Password: some_pass

Enter Password again to verify: some_pass

------------------

Added user "user1"

------------------

  User login: user1

  First name: Jim

  Last name: Wildman

  Full name: Jim Wildman

  Display name: Jim Wildman

  Initials: JW

  Home directory: /home/user1

  GECOS: Jim Wildman

  Login shell: /bin/sh

  Principal name: user1@RHPDS.OPENTLC.COM

  Principal alias: user1@RHPDS.OPENTLC.COM

  Email address: user1@rhpds.opentlc.com

  UID: 1723600001

  GID: 1723600001

  Password: True

  Member of groups: ipausers

  Kerberos keys available: True

|=====================================================

* We will add 4 users with specific UID’s and GID’s for later use.  The ipa user-add command will accept a password echo’d into the standard input to allow unattended usage.  The password for each user will be reset on first login +

[[t.6c2fe437cce0b739fc099cfe0da84ad2491a0381]][[t.15]]

[width="100%",cols="100%",]
|==========================================================================================================================================================
a|
[root@server1-<GUID> ~]# for i in 1 2 3 4; do echo "changeme" | ipa user-add classuser$i --password --first=User$i --last=Class --uid=1000$i; echo ""; done

-----------------------

Added user "classuser1"

-----------------------

  User login: classuser1

  First name: User1

  Last name: Class

  Full name: User1 Class

  Display name: User1 Class

  Initials: UC

  Home directory: /home/classuser1

  GECOS: User1 Class

  Login shell: /bin/sh

  Principal name: classuser1@RHPDS.OPENTLC.COM

  Principal alias: classuser1@RHPDS.OPENTLC.COM

  Email address: classuser1@rhpds.opentlc.com

  UID: 10001

  GID: 10001

  Password: True

  Member of groups: ipausers

  Kerberos keys available: True

[OMITTED]

|==========================================================================================================================================================

* Verify user creation

[[t.0edeaadea53ddddd9360508320fc13a3af8d1ad2]][[t.16]]

[width="100%",cols="100%",]
|==============================================
a|
[root@server1-<GUID> ~]# ipa user-find class

---------------

4 users matched

---------------

  User login: classuser1

  First name: User1

  Last name: Class

  Home directory: /home/classuser1

  Login shell: /bin/sh

  Principal name: classuser1@RHPDS.OPENTLC.COM

  Principal alias: classuser1@RHPDS.OPENTLC.COM

  Email address: classuser1@rhpds.opentlc.com

  UID: 10001

  GID: 10001

  Account disabled: False

[OMITTED]

----------------------------

Number of entries returned 4

----------------------------

|==============================================

[[h.3ughz3lq34l]]

* [[h.fg0gtwvje14a]]
Verify that you can login as classuser1.  You will be prompted to change the password.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

[[t.e22dc62e0bd58c9045446751555e7cfee628c3e4]][[t.17]]

[width="100%",cols="100%",]
|===================================================================================================
a|
[root@server1-<GUID> ~]# ssh classuser1@client2.example.com

The authenticity of host 'client2.example.com (<no hostip for proxy command>)' can't be established.

ECDSA key fingerprint is SHA256:MMIuQApTUiYZMSkpc+WlRklWCnXhmzCaIgtbE/Vb3UM.

ECDSA key fingerprint is MD5:b9:88:94:c2:f4:b6:7e:25:66:18:2f:25:99:49:7d:52.

Are you sure you want to continue connecting (yes/no)? yes

Warning: Permanently added 'client2.example.com' (ECDSA) to the list of known hosts.

Password:

Password expired. Change your password now.

Current Password:

New password:

Retype new password:

Creating home directory for classuser1.

-sh-4.2$ klist

Ticket cache: KEYRING:persistent:10001:krb_ccache_p7PlllC

Default principal: classuser1@RHPDS.OPENTLC.COM

Valid starting       Expires              Service principal

04/22/2018 08:13:12  04/23/2018 08:13:12  krbtgt/RHPDS.OPENTLC.COM@RHPDS.OPENTLC.COM

-sh-4.2$ hostname

client2-<GUID>.rhpds.opentlc.com

-sh-4.2$ logout

|===================================================================================================

* Verify that Single Sign On works

[[t.f4723265320cba316fee12d01e542015876086f6]][[t.18]]

[width="100%",cols="100%",]
|=========================================================================
a|
[root@server1-<GUID> ~]# ssh classuser1@client2-<GUID>.rhpds.opentlc.com

Password:

Last login: Sun May  6 22:10:55 2018 from server1-<GUID>.rhpds.opentlc.com

-sh-4.2$ hostname

client2-<GUID>.rhpds.opentlc.com

-sh-4.2$ ssh client3-<GUID>.rhpds.opentlc.com

-sh-4.1$ hostname

client3-<GUID>.rhpds.opentlc.com

-sh-4.1$ exit

logout

Connection to client3-<GUID>.rhpds.opentlc.com closed.

-sh-4.2$ exit

logout

Connection to client2-<GUID>.rhpds.opentlc.com closed.

|=========================================================================

* On the server1 generate ssh keys and upload them

[[t.a98937f4d637400e6070a72966390d64ae66aa6c]][[t.19]]

[width="100%",cols="100%",]
|=============================================================
a|
[root@server1-<GUID> ~]# ssh-keygen -t dsa -f ~/.ssh/classuser

Generating public/private dsa key pair.

Enter passphrase (empty for no passphrase): <ENTER>

Enter same passphrase again: <ENTER>

Your identification has been saved in /root/.ssh/classuser.

Your public key has been saved in /root/.ssh/classuser.pub.

The key fingerprint is:

[OMITTED]

|=============================================================

* cat /root/.ssh/classuser.pub then highlight and copy the contents of the key to your clipboard for use in the next step
* Paste the key into this command inplace of the text REPLACE THIS SAMPLE KEY. Depending on what characters end up in the key when it’s generated it may be necessary to replace the single ticks ‘  in this command with double ticks “, if one pair does not work try to the other but obviously they must match each other. If neither work then use an escaped double tick \” at either end of the string.

[[t.f5f26083b12575a55cf9b55ac5d84bb1a3cde922]][[t.20]]

[width="100%",cols="100%",]
|===========================================================================================================
a|
[root@server1-<GUID> ~]# for x in 1 2 3 4; do ipa user-mod classuser$x --sshpubkey='REPLACE THIS SAMPLE KEY’

done

--------------------------

Modified user "classuser1"

--------------------------

  User login: classuser1

  First name: User1

  Last name: Class

  Home directory: /home/classuser1

  Login shell: /bin/sh

  Principal name: classuser1@RHPDS.OPENTLC.COM

  Principal alias: classuser1@RHPDS.OPENTLC.COM

  Email address: classuser1@rhpds.opentlc.com

  UID: 10001

  GID: 10001

  SSH public key: ssh-dss [TRUNCATED]

  SSH public key fingerprint: [TRUNCATED]

  Account disabled: False

  Password: True

  Member of groups: ipausers

  Kerberos keys available: True

--------------------------

[OMITTED]

|===========================================================================================================

* From the server1 box, you can use the ssh key to login to any of the guest vm’s.

[[t.815af2d1e91496cb96ca059589fbe6d6c2404c83]][[t.21]]

[width="100%",cols="100%",]
|===========================================================================================
a|
[root@server1-<GUID> ~]# ssh -i ~/.ssh/classuser classuser1@client2-<GUID>.rhpds.opentlc.com

Last login: Sun May  6 22:11:53 2018 from server1-<GUID>.rhpds.opentlc.com

-sh-4.2$ 

Last login: Sun Apr 22 08:40:32 2018 from server1-<GUID>.rhpds.opentlc.com

-sh-4.2$ klist

Ticket cache: KEYRING:persistent:10001:krb_ccache_Pk86hbe

Default principal: classuser1@EXAMPLE.COM

Valid starting       Expires              Service principal

05/06/2018 22:12:14  05/07/2018 22:11:52  host/client3-<GUID>.rhpds.opentlc.com@EXAMPLE.COM

05/06/2018 22:11:52  05/07/2018 22:11:52  krbtgt/EXAMPLE.COM@EXAMPLE.COM

-sh-4.2$ exit +
logout +
Connection to client2-<GUID>.rhpds.opentlc.com closed.

|===========================================================================================

* We are going to create 2 user groups to demonstrate host based access control.  We will put the odd users in one group and the even users in another.

[[t.18662dead2911cb1dcd984b4812bbd6ffc64b442]][[t.22]]

[width="100%",cols="100%",]
|=================================================================================
a|
[root@server1-<GUID> ~]# ipa group-add classevenusers

----------------------------

Added group "classevenusers"

----------------------------

  Group name: classevenusers

  GID: 1723600003

[root@server1-<GUID> ~]# ipa group-add classoddusers

---------------------------

Added group "classoddusers"

---------------------------

  Group name: classoddusers

  GID: 1723600004

[root@server1-<GUID> ~]# ipa group-add-member classevenusers --users=classuser2

  Group name: classevenusers

  GID: 1723600003

  Member users: classuser2

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa group-add-member classevenusers --users=classuser4

  Group name: classevenusers

  GID: 1723600003

  Member users: classuser2, classuser4

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa group-add-member classoddusers --users=classuser1

  Group name: classoddusers

  GID: 1723600004

  Member users: classuser1

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa group-add-member classoddusers --users=classuser3

  Group name: classoddusers

  GID: 1723600004

  Member users: classuser1, classuser3

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa group-find class --all

----------------

2 groups matched

----------------

  dn: cn=classevenusers,cn=groups,cn=accounts,dc=rhpds,dc=opentlc,dc=com

  Group name: classevenusers

  GID: 1723600003

  Member users: classuser2, classuser4

  ipauniqueid: 0f8dbd04-464e-11e8-857c-2cc26020e991

  objectclass: top, groupofnames, nestedgroup, ipausergroup, ipaobject, posixgroup

  dn: cn=classoddusers,cn=groups,cn=accounts,dc=rhpds,dc=opentlc,dc=com

  Group name: classoddusers

  GID: 1723600004

  Member users: classuser1, classuser3

  ipauniqueid: 1813ab82-464e-11e8-8af1-2cc26020e991

  objectclass: top, groupofnames, nestedgroup, ipausergroup, ipaobject, posixgroup

----------------------------

Number of entries returned 2

----------------------------

|=================================================================================

* Create hostgroups for the even and odd hosts

[[t.92122a737fb0714f71056ca7212b626be0730bb0]][[t.23]]

[width="100%",cols="100%",]
|========================================================================================================
a|
[root@server1-<GUID> ~]# ipa hostgroup-add classevenhosts

--------------------------------

Added hostgroup "classevenhosts"

--------------------------------

  Host-group: classevenhosts

[root@server1-<GUID> ~]# ipa hostgroup-add classoddhosts

ipa hostgroup-add classoddhosts

-------------------------------

Added hostgroup "classoddhosts"

-------------------------------

  Host-group: classoddhosts

[root@server1-<GUID> ~]# ipa hostgroup-add-member classevenhosts --hosts=client2-<GUID>.rhpds.opentlc.com

  Host-group: classevenhosts

  Member hosts: client2-<GUID>.rhpds.opentlc.com

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa hostgroup-add-member classevenhosts --hosts=client4-<GUID>.rhpds.opentlc.com

  Host-group: classevenhosts

  Member hosts: client2-<GUID>.rhpds.opentlc.com, client4-<GUID>.rhpds.opentlc.com

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa hostgroup-add-member classoddhosts --hosts=client1-<GUID>.rhpds.opentlc.com

  Host-group: classoddhosts

  Member hosts: client1-<GUID>.rhpds.opentlc.com

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa hostgroup-add-member classoddhosts --hosts=client3-<GUID>.rhpds.opentlc.com

  Host-group: classoddhosts

  Member hosts: client1-<GUID>.rhpds.opentlc.com, client3-<GUID>.rhpds.opentlc.com

-------------------------

Number of members added 1

-------------------------

|========================================================================================================

* Verify that the groups were created correctly

[[t.fbd7f7858fecd2d58cfdd07021e75dbafd7293e7]][[t.24]]

[width="100%",cols="100%",]
|=====================================================================================
a|
[root@server1-<GUID> ~]# ipa hostgroup-find class --all

--------------------

2 hostgroups matched

--------------------

  dn: cn=classevenhosts,cn=hostgroups,cn=accounts,dc=example,dc=com

  Host-group: classevenhosts

  Member hosts: client2-<GUID>.rhpds.opentlc.com, client4-<GUID>.rhpds.opentlc.com

  ipauniqueid: b7f4c1ee-4fbe-11e8-a786-2cc26020e991

  mepmanagedentry: cn=classevenhosts,cn=ng,cn=alt,dc=example,dc=com

  objectclass: ipaobject, ipahostgroup, nestedGroup, groupOfNames, top, mepOriginEntry

  dn: cn=classoddhosts,cn=hostgroups,cn=accounts,dc=example,dc=com

  Host-group: classoddhosts

  Member hosts: client1-<GUID>.rhpds.opentlc.com, client3-<GUID>.rhpds.opentlc.com

  ipauniqueid: ba1b1c3e-4fbe-11e8-b0b4-2cc26020e991

  mepmanagedentry: cn=classoddhosts,cn=ng,cn=alt,dc=example,dc=com

  objectclass: ipaobject, ipahostgroup, nestedGroup, groupOfNames, top, mepOriginEntry

----------------------------

Number of entries returned 2

----------------------------

|=====================================================================================

[[h.kqh1kn11mfmw]]
Lab 4: Configure sudo
~~~~~~~~~~~~~~~~~~~~~

* We will configure sudo to allow members of the classevenusers group (from Lab 3) to run a few commands as root.
* Add commands to be run via sudo

[[t.b2efd321ae2e8b38447d5ca5a728f9b1bc5711b4]][[t.25]]

[width="100%",cols="100%",]
|===========================================================
|[root@server1-<GUID> ~]# ipa sudocmd-add /usr/sbin/visudo +
------------------------------------- +
Added Sudo Command "/usr/sbin/visudo" +
------------------------------------- +
 Sudo Command: /usr/sbin/visudo +
[root@server1-<GUID> ~]# ipa sudocmd-add /usr/bin/yum +
--------------------------------- +
Added Sudo Command "/usr/bin/yum" +
--------------------------------- +
 Sudo Command: /usr/bin/yum
|===========================================================

* Create a rule to manage the commands and the users

[[t.aab1ee81f6de51ad972cdcdbad04252f1571c256]][[t.26]]

[width="100%",cols="100%",]
|======================================================
a|
[root@server1-<GUID> ~]# ipa sudorule-add labcommands +
-----------------------------

Added Sudo Rule "labcommands"

-----------------------------

  Rule name: labcommands

  Enabled: TRUE

|======================================================

* Add the commands and the users to the rule

[[t.b2169b638db4525e5599df61b541bbc9de589734]][[t.27]]

[width="100%",cols="100%",]
|================================================================================================
a|
[root@server1-<GUID> ~]# ipa sudorule-add-allow-command labcommands --sudocmds=/usr/bin/yum +
  Rule name: labcommands +
 Enabled: TRUE +
 Sudo Allow Commands: /usr/bin/yum +
------------------------- +
Number of members added 1 +
------------------------- +
[root@server1-<GUID> ~]# ipa sudorule-add-allow-command labcommands --sudocmds=/usr/sbin/visudo +
 Rule name: labcommands +
 Enabled: TRUE +
 Sudo Allow Commands: /usr/bin/yum, /usr/sbin/visudo +
------------------------- +
Number of members added 1 +
------------------------- +
[root@server1-<GUID> ~]# ipa sudorule-add-runasuser labcommands --users=root +
  Rule name: labcommands +
 Enabled: TRUE +
 Sudo Allow Commands: /usr/bin/yum, /usr/sbin/visudo +
 RunAs External User: root +
------------------------- +
Number of members added 1 +
-------------------------

[root@server1-<GUID> ~]# ipa sudorule-add-user --group=classevenusers +
Rule name: labcommands +
 Rule name: labcommands +
  Enabled: TRUE +
 User Groups: classevenusers +
 Sudo Allow Commands: /usr/bin/yum, /usr/sbin/visudo +
 RunAs External User: root +
------------------------- +
Number of members added 1 +
-------------------------

[root@server1-<GUID> ~]# ipa sudorule-find +
------------------- +
1 Sudo Rule matched +
------------------- +
 Rule name: labcommands +
 Enabled: TRUE +
 RunAs External User: root +
---------------------------- +
Number of entries returned 1 +
----------------------------

|================================================================================================

* Test the new rule by logging into client2 as classuser2 and running the commands

[[t.c67701f0511e18d286feebee36cb89c7e5982f21]][[t.28]]

[width="100%",cols="100%",]
|=================================================================================================
a|
[root@server1-<GUID> ~]# ssh classuser2@client2-<GUID>.rhpds.opentlc.com

Password: changeme 

Password expired. Change your password now.

Current Password:

New password:

Retype new password:

Creating home directory for classuser2.

Last failed login: Sun May  6 22:48:05 EDT 2018 from server1-<GUID>.rhpds.opentlc.com on ssh:notty

There were 3 failed login attempts since the last successful login.

-sh-4.2$ sudo /usr/sbin/visudo 

We trust you have received the usual lecture from the local System

Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.

    #2) Think before you type.

    #3) With great power comes great responsibility.

[sudo] password for classuser2:

classuser2 is not allowed to run sudo on client2-<GUID>.  This incident will be reported.

-sh-4.2$ exit

logout

Connection to client2-893a.rhpds.opentlc.com closed.

|=================================================================================================

* Why did the command fail?
* Add the classevenhosts host group to the command

[[t.0e525cf96eca731604d14b0ee47030fdbf63696f]][[t.29]]

[width="100%",cols="100%",]
|============================================================================================
a|
[root@server1-<GUID> ~]# ipa sudorule-add-host labcommands

[member host]: <ENTER>

[member host group]: classevenhosts

  Rule name: labcommands

  Enabled: TRUE

  User Groups: classevenusers

  Host Groups: classevenhosts

  Sudo Allow Commands: /usr/bin/yum, /usr/sbin/visudo

  RunAs External User: root

-------------------------

Number of members added 1

-------------------------

[root@server1-<GUID> ~]# ipa sudorule-find --all

-------------------

1 Sudo Rule matched

-------------------

  dn: ipaUniqueID=5aad309e-519d-11e8-9846-2cc26020e991,cn=sudorules,cn=sudo,dc=example,dc=com

  Rule name: labcommands

  Enabled: TRUE

  User Groups: classevenusers

  Host Groups: classevenhosts

  Sudo Allow Commands: /usr/bin/yum, /usr/sbin/visudo

  RunAs External User: root

  ipauniqueid: 5aad309e-519d-11e8-9846-2cc26020e991

  objectclass: ipaassociation, ipasudorule

----------------------------

Number of entries returned 1

----------------------------

|============================================================================================

* Log into an even number host with an even numbered user id (if you are already logged in this this way you will need to log out and back in to start a new session) and then execute the commands that were added.

[[t.3265c44aa4c5f09051961e1761065f63467c3734]][[t.30]]

[width="100%",cols="100%",]
|=======================================================================
a|
[root@server1-<GUID> ~]# ssh classuser2@client2-<GUID>.rhpds.opentlc.com

-sh-4.2$ sudo /usr/sbin/visudo

\{edit works}

-sh-4.2$ sudo /usr/bin/yum -y install gcc

\{install works}

|=======================================================================

* If there are issues, login as root and clear the sssd cache

[[t.a0917697d6b441984ed6b08cdbe57f26ad9d1510]][[t.31]]

[width="100%",cols="100%",]
|=========================================================================
a|
[root@server1-<GUID> ~]# ssh client2-<GUID>.rhpds.opentlc.com

Password:

Last login: Sun May  6 22:54:51 2018 from server1-<GUID>.rhpds.opentlc.com

[root@client2-<GUID> ~]# sss_cache -E

[root@client2-<GUID> ~]#

|=========================================================================

 +
         then repeat the sudo test

[[t.54bc48bc30c6b264a5eed93847296ed5babc459a]][[t.32]]

[width="100%",cols="100%",]
|============================================================
a|
[root@server1-<GUID> ~]# ssh client2-<GUID>.rhpds.opentlc.com

-sh-4.2$ sudo /usr/sbin/visudo

\{edit works}

-sh-4.2$ sudo /usr/bin/yum -y install gcc

\{install works}

|============================================================

[[h.gkcp4uvw9zpn]]
Lab 5: Configure Host Based Access Control
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Host Based Access Control (HBAC) enables you to restrict which users have access to which hosts or groups of hosts.  We are going to configure HBAC to allow classevenusers to access classevengroup hosts,, and classoddusers to access classoddgroup hosts
* Create the odd to odd rule

[[t.cdc2e54c06530fe91ab476c69daa98a5e21d427b]][[t.33]]

[width="100%",cols="100%",]
|=========================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add class_odd_to_odd

----------------------------------

Added HBAC rule "class_odd_to_odd"

----------------------------------

  Rule name: class_odd_to_odd

  Enabled: TRUE

|=========================================================

* Add the hosts

[[t.d195fef5af50fa4fff5430ab87928b5f7e5f3d57]][[t.34]]

[width="100%",cols="100%",]
|=========================================================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add-host class_odd_to_odd --hostgroups=classoddhosts

  Rule name: class_odd_to_odd

  Enabled: TRUE

  Host Groups: classoddhosts

-------------------------

Number of members added 1

-------------------------

|=========================================================================================

* Add the users

[[t.7326e709bd1d707b07598ce3bb9c258a148c4260]][[t.35]]

[width="100%",cols="100%",]
|=====================================================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add-user class_odd_to_odd --groups=classoddusers

  Rule name: class_odd_to_odd

  Enabled: TRUE

  User Groups: classoddusers

  Host Groups: classoddhosts

-------------------------

Number of members added 1

|=====================================================================================

* Restrict the access to ssh only

[[t.8263595f704bd6bc2c2332d323dad0456a4ed020]][[t.36]]

[width="100%",cols="100%",]
|=================================================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add-service class_odd_to_odd --hbacsvcs=sshd

  Rule name: class_odd_to_odd

  Enabled: TRUE

  User Groups: classoddusers

  Host Groups: classoddhosts

  Services: sshd

-------------------------

Number of members added 1

-------------------------

|=================================================================================

* Create the even to even rule

[[t.0ec632bb7d69874e9fa1793a47091bb3d96c5479]][[t.37]]

[width="100%",cols="100%",]
|===========================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add class_even_to_even

------------------------------------

Added HBAC rule "class_even_to_even"

------------------------------------

  Rule name: class_even_to_even

  Enabled: TRUE

|===========================================================

* Add the hosts

[[t.45c46441ccc10d9197a953be0deffcf2ef81b447]][[t.38]]

[width="100%",cols="100%",]
|============================================================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add-host class_even_to_even --hostgroups=classevenhosts

  Rule name: class_even_to_even

  Enabled: TRUE

  Host Groups: classevenhosts

-------------------------

Number of members added 1

-------------------------

|============================================================================================

* Add the users

[[t.344a273f8e078721cbd511fffe8d8a54fcb1b1a7]][[t.39]]

[width="100%",cols="100%",]
|========================================================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add-user class_even_to_even --groups=classevenusers

  Rule name: class_even_to_even

  Enabled: TRUE

  User Groups: classevenusers

  Host Groups: classevenhosts

-------------------------

Number of members added 1

-------------------------

|========================================================================================

* Restrict it to ssd access

[[t.af82adbc93c32f1857fd7824961d7db1dde667d7]][[t.40]]

[width="100%",cols="100%",]
|===================================================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-add-service class_even_to_even --hbacsvcs=sshd

  Rule name: class_even_to_even

  Enabled: TRUE

  User Groups: classevenusers

  Host Groups: classevenhosts

  Services: sshd

-------------------------

Number of members added 1

-------------------------

|===================================================================================

* Display the rules.   Anybody see a problem?

[[t.5648606ffa32c4e65accb2b0b4bfcf660241a0a5]][[t.41]]

[width="100%",cols="100%",]
|==============================================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-find

--------------------

3 HBAC rules matched

--------------------

  Rule name: allow_all

  User category: all

  Host category: all

  Service category: all

  Description: Allow all users to access any host from any host

  Enabled: TRUE

  Rule name: class_even_to_even

  Enabled: TRUE

  Rule name: class_odd_to_odd

  Enabled: TRUE

----------------------------

Number of entries returned 3

----------------------------

|==============================================================

* Disable the allow all rule

[[t.0fc340fa4a8ebcb5f77fc92084d474647e68e65c]][[t.42]]

[width="100%",cols="100%",]
|============================================
a|
[root@server1-<GUID> ~]# ipa hbacrule-disable

Rule name: allow_all

------------------------------

Disabled HBAC rule "allow_all"

------------------------------

|============================================

* Test the rules

[[t.c911428da084d8b1d92e48b5380ba8813b2d9084]][[t.43]]

[width="100%",cols="100%",]
|=========================================================================
a|
\{even user to even host - succeed}

[root@server1-<GUID> ~]# ssh classuser2@client2-<GUID>.rhpds.opentlc.com

Password:

Last login: Sun May  6 22:55:10 2018 from server1-<GUID>.rhpds.opentlc.com

-sh-4.2$ exit

logout

Connection to client2-<GUID>.rhpds.opentlc.com closed.

\{even user to odd host - fail}

[root@server1-<GUID> ~]# ssh classuser2@client1-<GUID>.rhpds.opentlc.com

Password:

Authentication failed.

\{odd user to odd host - succeed}

[root@server1-<GUID> ~]# ssh classuser1@client1-<GUID>.rhpds.opentlc.com

Password:

Creating home directory for classuser1.

-sh-4.2$ exit

logout

Connection to client1-<GUID>.rhpds.opentlc.com closed.

\{odd user to even host -fail}

[root@server1-<GUID> ~]# ssh classuser1@client2-<GUID>.rhpds.opentlc.com

Password:

Authentication failed.

|=========================================================================

[[h.hjz5sxl2emrv]]
Lab 6: Configure IdM Replication
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* IdM is backed by a multimaster, LDAP database.  RHEL supports up to 60 IdM replicas out of the box with RHEL 6 and 7 clients.  We are going to configure a simple 2 node replica
* Starting in RHEL 7.3, the replication installation process changed.

* Register as a client
* Run ipa-replica-install to promote to a replica
* We will use server2 as our new replica

* Install the ipa-server package on server2

[[t.3ba86267c8f38aa0a0bdc3894683315d89de9d9d]][[t.44]]

[width="100%",cols="100%",]
|=====================================================================
|[root@server2-<GUID>.rhpds.opentlc.com ~]# yum -y install ipa-server 
|=====================================================================

* Open up the firewall rules
* This needs to be done before we install the replica because the installation process will test that the ports are open for replication

[[t.181600226d98368980332452accaf7fda74ad8bd]][[t.45]]

[width="100%",cols="100%",]
|==================================================================================================================================================================
a|
[root@server2-<GUID>.rhpds.opentlc.com]# firewall-cmd --permanent --add-port=\{80/tcp,443/tcp,389/tcp,636/tcp,88/tcp, 464/tcp,53/tcp,88/udp,464/udp,53/udp,123/udp}

[root@server2-GUIID.rhpds.opentlc.com]# firewall-cmd --reload

|==================================================================================================================================================================

* Install the replica.  There is no need to provide the domain, server or realm since as a client it already knows those things

[[t.193016d2fd718bac41ea88d6d0ef777b04beb5b2]][[t.46]]

[width="100%",cols="100%",]
|====================================================================
a|
[root@server2-<GUID> ~]# ipa-replica-install

WARNING: conflicting time&date synchronization service 'chronyd' will

be disabled in favor of ntpd

Password for admin@EXAMPLE.COM:

Run connection check to master

Connection check OK

Configuring NTP daemon (ntpd)

\{omitted}

Upgrading IPA:. Estimated time: 1 minute 30 seconds

  [1/9]: stopping directory server

[OMITTED]

  [9/9]: starting directory server

Done.

Restarting the KDC

|====================================================================

* Verify that replication is working by checking for the user ‘test’ on server1, then adding user “test” from server2 and rechecking.

[[t.159ca45fbb35e8562f83c44f001ae0521216b11b]][[t.47]]

[width="100%",cols="100%",]
|=============================================================
a|
[root@server1-<GUID>.rhpds.opentlc.com ~]# ipa user-show test 

ipa: ERROR: test: user not found

\{on server2}

[root@server2-<GUID>.rhpds.opentlc.com ~]# kinit admin

Password for admin@rhpds.opentlc.com: changeme

[[root@server2-<GUID>.rhpds.opentlc.com ~]# ipa user-add test

First name: Jim

Last name: Wildman

-----------------

Added user "test"

-----------------

  User login: test

  First name: Jim

  Last name: Wildman

  Full name: Jim Wildman

[OMITTED]

\{back on server1}

[root@server1-<GUID>.rhpds.opentlc.com ~]# ipa user-show test

  User login: test

  First name: Jim

  Last name: Wildman

[OMITTED]

|=============================================================

* Open the browser to https://www.google.com/url?q=https://server1-guid.rhpds.opentlc.com&sa=D&ust=1554498733554000[https://server1-<GUID>.rhpds.opentlc.com]

* Select “IPA Server”
* Select “Topology”
* Select “Topology Graph” from left menu

* Additional replicas can be added from here, both identity and CA replicas.

[[h.58mjb4whloeh]]
Lab 7: Configure automount
~~~~~~~~~~~~~~~~~~~~~~~~~~

Automount allows a single home directory to be shared across all machines in the environment.  The shared directory is automatically mounted to a machine when the user logs in.  IdM can be used to store the configurations.   IdM provides Kerberized tokens to enable secure mounting of NFSv4 home directories.  For automount to work correctly with IdM, the machine providing the home directories must be a member of the IdM Kerberos realm.  We will use client1 to host the NFS shares.  If you are using a NAS filer then it will need to be registered with IdM as well.  You can use the IdM server itself, but that is just an added complexity.

* On server1, add all 4 client vm’s as service principals for nfs services

[[t.efe23a33e69046a71e81ce12c27a2e1a349a1f28]][[t.48]]

[width="100%",cols="100%",]
|========================================================================================================
a|
[root@server1-<GUID> ~]# for i in 1 2 3 4; do ipa service-add nfs/client$i-<GUID>.rhpds.opentlc.com; done

--------------------------------------------------------------

Added service "nfs/client1-<GUID>.rhpds.opentlc.com@EXAMPLE.COM"

--------------------------------------------------------------

  Principal name: nfs/client1-<GUID>.rhpds.opentlc.com@EXAMPLE.COM

  Principal alias: nfs/client1-<GUID>.rhpds.opentlc.com@EXAMPLE.COM

  Managed by: client1-<GUID>.rhpds.opentlc.com

[OMITTED]

|========================================================================================================

* We’ll do the rest of the work from client1, which will be our nfs server as well

[[t.0ff2ba40ecadceecfe2795b348edea2c3e960d81]][[t.49]]

[width="100%",cols="100%",]
|=====================================================================================================================================
a|
[root@client1-<GUID> ~]# kinit admin

Password for admin@EXAMPLE.COM:

\{get the keytab}

[root@client1-<GUID> ~]# ipa-getkeytab -s server1-<GUID>.rhpds.opentlc.com -p nfs/client1-<GUID>.rhpds.opentlc.com -k /etc/krb5.keytab

Keytab successfully retrieved and stored in: /etc/krb5.keytab

\{enable firewall for nfs}

[root@client1-<GUID> ~]# for i in nfs mountd rpc-bind; do firewall-cmd --add-service=$i --permanent; done

success

success

Success

[root@client1-<GUID> ~]# firewall-cmd --reload

Success

\{configure the ipa client}

[root@client1-<GUID> ~]# ipa-client-automount --server server1-<GUID>.rhpds.opentlc.com

IPA server: server1-<GUID>.rhpds.opentlc.com

Location: default

Continue to configure the system with these values? [no]: yes

Configured /etc/sysconfig/nfs  

Configured /etc/idmapd.conf

Started rpcidmapd

Started rpcgssd

Restarting sssd, waiting for it to become available.

Started autofs

|=====================================================================================================================================

* Setup client1 as an nfs server and create the user directories

[[t.4e993cc20943138d95fa636d5954ebcde146ddba]][[t.50]]

[width="100%",cols="100%",]
|=======================================================================================================================================================================================
a|
[root@client1-<GUID> ~]# mkdir -p /export/home

[root@client1-<GUID> ~]# echo ‘/export/home *(rw,sec=krb5:krb5i:krb5p)’ >> /etc/exports

[root@client1-<GUID> ~]# exportfs -ra

[root@client1-<GUID> ~]# showmount -e localhost

Export list for localhost:

/export/home *

[root@client1-<GUID> ~]# for i in nfs nfs-server nfs-secure; do systemctl enable $i;systemctl start $i; done

[root@client1-<GUID> ~]# for i in 1 2 3 4; do mkdir /export/home/classuser$i; touch /export/home/classuser$i/hello_world;chown -R classuser$i.classuser$i /export/home/classuser$i; done

|=======================================================================================================================================================================================

* Add the final ipa pieces

[[t.f54371e9dfc1c1c19c040eb007234f0021a0081f]][[t.51]]

[width="100%",cols="100%",]
|=================================================================================================================================
a|
[root@client1-<GUID> ~]# ipa automountmap-add-indirect default auto.home --mount=/home

-------------------------------

Added automount map "auto.home"

-------------------------------

  Map: auto.home

[root@client1-<GUID> ~]# ipa automountkey-add default auto.home --key "*" --info "client1-<GUID>.rhpds.opentlc.com:/export/home/&"

-----------------------

Added automount key "*"

-----------------------

  Key: *

  Mount information: client1-<GUID>.rhpds.opentlc.com:/export/home/&

|=================================================================================================================================

* Each client must be locally configure.  There are different commands for RHEL 6 and 7

[[t.804935e1e29ddcffd5b48158e84c08cdf4ba2348]][[t.52]]

[width="100%",cols="100%",]
|===========================================================================================================================
a|
\{client1 and 2 - RHEL 7}

[root@client2-<GUID> ~]# ipa-client-automount --server server1-<GUID>.rhpds.opentlc.com

IPA server: server1-<GUID>.rhpds.opentlc.com

Location: default

Continue to configure the system with these values? [no]: yes

Configured /etc/nsswitch.conf

Configured /etc/sysconfig/nfs

Configured /etc/idmapd.conf

Started rpcidmapd

Started rpcgssd

Restarting sssd, waiting for it to become available.

Started autofs

[root@client2-<GUID> ~]# for i in rpc-gssd rpcbind nfs-idmapd; do systemctl enable $i; systemctl start $i; done

Created symlink from /etc/systemd/system/multi-user.target.wants/rpcbind.service to /usr/lib/systemd/system/rpcbind.service.

\{client3 and 4 - RHEL 6}

[root@client4-<GUID> ~]# ipa-client-automount --server server1-<GUID>.rhpds.opentlc.com

IPA server: server1-<GUID>.rhpds.opentlc.com

Location: default

Continue to configure the system with these values? [no]: yes

Configured /etc/nsswitch.conf

Configured /etc/sysconfig/nfs

Configured /etc/idmapd.conf

Started rpcidmapd

Started rpcgssd

Restarting sssd, waiting for it to become available.

Started autofs

[root@client4-<GUID> ~]# chkconfig nfs on

[root@client4-<GUID> ~]# service nfs restart

Shutting down NFS daemon:                                  [FAILED]

Shutting down NFS mountd:                                  [FAILED]

Shutting down RPC svcgssd:                                 [FAILED]

Starting RPC svcgssd:                                      [FAILED]

Starting NFS services:                                     [  OK  ]

Starting NFS mountd:                                       [  OK  ]

Starting NFS daemon:                                       [  OK  ]

[root@client4-<GUID> ~]#                                     [  OK  ]

|===========================================================================================================================

* Login to the clients and confirm that the home directory mounts correctly
* You should be able to create files from any of the clients

[[t.2fc0693c3bf600927e184cf0c7b4e4a4d6d86a46]][[t.53]]

[width="100%",cols="100%",]
|=====================================================================
a|
[root@server1-<GUID> ~]# ssh classuser1@client4.example.com

-sh-4.1$ df

Filesystem           1K-blocks    Used Available Use% Mounted on

/dev/vda1             10188648  923436   8744368  10% /

tmpfs                   961036       0    961036   0% /dev/shm

client1-<GUID>.rhpds.opentlc.com:/export/home/classuser1

                      10474496 1492992   8981504  15% /home/classuser1

-sh-4.1$ ll

total 0

-rw-r--r--. 1 classuser1 classuser1 0 May  7 13:22 hello_world

|=====================================================================

CONGRATULATIONS!!
